<!--
Pós-Graduação Unisal 2017
Módulo: Design para Web e Mobile
Atividade Prática de Projeto Contextualizado
Pagina HTML para atividade em sala de aula
Projeto de desenvolvimento de layout de página responsivo e utilizando a técnica de form wizard
Página de Formulário para coleta de dados de Cotação Online de Seguros de Automóveis

Frameworks utilizados:
Bootstrap: Design, resposividade e comportamento das páginas
Angular: Comportamento e integrações com APIs web services rest

Gulp: Automatizados de tarefas de empacotamento e otimizações de código
NPM: Gerenciador de dependencias e pacotes

-->

Necessário:
  nodejs v8.10.0
  npm 5
  gulp-cli

Estando dentro da pasta do projeto:

iniciar o projeto e baixar as dependencias:
  $> npm install

empacotar o projeto:
    $> gulp default

rodar o projeto em modo desenvolvimento com servidor habilitado
    $> gulp dev-mode  

rodar o projeto em modo servidor:
    $> gulp server
