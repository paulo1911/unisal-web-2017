// Aqui nós carregamos o gulp e os plugins através da função `require` do nodejs
var del = require('del');
var gulp = require( 'gulp' );
var cleanCSS = require('gulp-clean-css');
var uglifyJS = require('gulp-uglify');
var cleanHTML = require('gulp-htmlmin');
var notify = require( 'gulp-notify' );
var browserSync = require('browser-sync').create();

// Definimos o diretorio dos arquivos para evitar repetição futuramente
var BUILD_PATH = './build'
var JS_FILES = "./src/js/**/*.js";
var CSS_FILES = "./src/css/**/*.css";
var HTML_FILES = "./src/**/*.html";
var WHATCH_FILES = __dirname + '/src/**';

/**
* Task basica de limpeza da pasta de destino
* dos artefatos gerados no build
**/
gulp.task('limpar', function () {
	del.sync(__dirname + BUILD_PATH + '/**');
});

/**
* Tarefa padrão para gerar nossos artefatos JS
**/
gulp.task('js', function(){
  gulp.src(JS_FILES)
    .pipe( uglifyJS() )
    .pipe( gulp.dest( BUILD_PATH + '/js' ) )
    .pipe( notify('JS Scripts OK!'));
});

/**
* Tarefa padrão para gerar nossos artefatos CSS
**/
gulp.task('css', function(){
  gulp.src(CSS_FILES)
    .pipe( cleanCSS() )
    .pipe( gulp.dest( BUILD_PATH + '/css' ) )
    .pipe( notify('CSS Styles OK!'));
});


/**
* Tarefa padrão para gerar nossos artefatos HTML
**/
gulp.task('html', function () {
    gulp.src(HTML_FILES)
				.pipe( cleanHTML({collapseWhitespace: true, minifyJS:true,removeComments:true}) )
        .pipe(gulp.dest(BUILD_PATH))
            .pipe( notify('HTML Files OK!'));
});

/**
* Tarefa para incluir artefatos específicos no nosso projeto
**/
gulp.task("libs", function () {
    gulp.src("./node_modules/bootstrap/dist/**").pipe(gulp.dest(BUILD_PATH + '/lib/bootstrap'));
    gulp.src("./node_modules/jquery/dist/**").pipe(gulp.dest(BUILD_PATH + '/lib/jquery'));
		gulp.src("./node_modules/angular/**").pipe(gulp.dest(BUILD_PATH + '/lib/angular'));
});

/**
* Tarefa padrão do nosso projeto gulp
* limpar pasta do projeto e gerar os artefatos novamente
* na pasta de destino
**/
gulp.task('default', ['limpar','js', 'css', 'libs','html'], function() {

});

/**
* Tarefa padrão do nosso projeto gulp EM MODO DEV
* limpar pasta do projeto e gerar os artefatos novamente
*na pasta de destino
**/
gulp.task('dev-mode', ['default', 'server'], function() {

  //observa se arquivos de código foram alterados em desenvolvimento e gera novos pacotes para visualizar no browser
  gulp.watch(WHATCH_FILES, ['default'], function(){

  });;
});

/**
* Tarefa padrão do nosso projeto para criar e inicializar nosso servidor HTTP local
* O servidor local é necessário e nos ajuda a testar nosso projeto no browser
* url do servidor local: http://localhost:3000
**/
gulp.task('server', function() {
  browserSync.init([BUILD_PATH + '/**'], {
      server: {
          baseDir: BUILD_PATH, //pasta raiz do projeto no servidor
          index: 'index.html', //página inicial do projeto
					reloadDebounce: 2000
      }
  });

  //observa se arquivos de código foram atualizados no servidor local e recarrega o browser
  gulp.watch(BUILD_PATH).on('change', function(){
    console.log('Reloading browser...');
    browserSync.reload(); //recarrega o navegador
  });
});
